/*----------------------------------------------------------------------------
  The RedPill library for RGB LED matrices, is designed to provide an
  object-oriented framework for 2-dimentional RGB LED strip-based matrices.
  It relies on Adafruit Industries' NeoPixel library and provides an
  abstraction layer allowing the user to interact with the matrix as 
  'a matrix' instead of a criss-crossed strip of LEDs.

  ----------------------------------------------------------------------------
  This file is part of the RedPill library.

  Licence: MIT/X11 (see LICENCE file)
  Author: 0x783czar (Diego Carrión)
  ----------------------------------------------------------------------------*/

#include "RedPill.h"

RedPill::RedPill(uint8_t x, uint8_t y, uint8_t pin, uint8_t type)
{
  this->xAxisCount = x;
  this->yAxisCount = y;
  uint16_t total_leds = this->xAxisCount * this->yAxisCount;

  Adafruit_NeoPixel adafruit_strip = Adafruit_NeoPixel(total_leds, pin, type);
  this->strip = &adafruit_strip;
  this->strip->setBrightness(15);
  
  /* Initialize 2-dimentional array with Pixel objects */
  this->matrix = (RPPixel**)malloc(sizeof(RPPixel**) * this->xAxisCount);
  for (int i = 0; i < this->xAxisCount; ++i)
  {
    matrix[i] = (RPPixel*)malloc(sizeof(RPPixel*) * this->yAxisCount);
    for (int j = 0; j < this->yAxisCount; ++j)
    {
      // Identify Serial Number for LED in question
      uint16_t serial;
      if (i % 2 == 0)
      {
        serial = this->xAxisCount * i + j;
      }
      else
      {
        serial = this->xAxisCount * (i + 1) - (j + 1);
      }
      // Add LED Pixel object to array
      this->matrix[i][j] = RPPixel(i, j, serial, strip);
    }
  }
}

RedPill::~RedPill()
{
  // deconstructor
}

void RedPill::on()
{
  this->strip->begin();
}

void RedPill::begin()
{
  this->on();
}

void RedPill::paint()
{
  this->strip->show();
}

void RedPill::show()
{
  this->paint();
}

void RedPill::setPixel(uint8_t x, uint8_t y, uint32_t color)
{
  // Ensure values do not extend beyond the bounds of the array
  if (x < this->xAxisCount && y < this->yAxisCount)
  {
    this->matrix[x][y].setColor(color);
  }
  else if (Serial)
  {
    Serial.println("ERROR: pixel out of bounds");
  }
}

void RedPill::setPixel(uint8_t x, uint8_t y, uint8_t r, uint8_t g, uint8_t b)
{
  // Ensure values do not extend beyond the bounds of the array
  if (x < this->xAxisCount && y < this->yAxisCount)
  {
    this->matrix[x][y].setColor(r, g, b);
  }
  else if (Serial)
  {
    Serial.println("ERROR: pixel out of bounds");
  }
}

void RedPill::setBrightness(uint8_t brightness)
{
  this->strip->setBrightness(brightness);
}

uint8_t RedPill::getXAxisCount()
{
  return this->xAxisCount;
}

uint8_t RedPill::getYAxisCount()
{
  return this->yAxisCount;
}

// RPPixel** RedPill::getMatrix()
// {
//   return this->matrix;
// }

// RPPixel* RedPill::getPixel(uint8_t x, uint8_t y)
// {
//   return &(this->matrix[x][y]);
// }

uint32_t RedPill::Color(uint8_t r, uint8_t g, uint8_t b)
{
  return Adafruit_NeoPixel::Color(r, g, b);
}

