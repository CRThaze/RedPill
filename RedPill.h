/*----------------------------------------------------------------------------
  This file is part of the RedPill library.

  Licence: MIT/X11 (see LICENCE file)
  Author: 0x783czar (Diego Carrión)
  ----------------------------------------------------------------------------*/

#include "RPPixel.h"

/* short-hand representations for common colors */
#define WHITE RedPill::Color(255, 255, 255)
#define RED RedPill::Color(255, 0, 0)
#define ORANGE RedPill::Color(255, 255 / 2, 0)
#define YELLOW RedPill::Color(255, 255 - (255 / 5), 0)
#define GREEN RedPill::Color(0, 255, 0)
#define BLUE RedPill::Color(0, 0, 255)
#define INDIGO RedPill::Color(128, 0, 255 - (255 / 5))
#define VIOLET RedPill::Color(255, 0, 255)
#define BROWN RedPill::Color(139, 69, 19)
#define BLACK 0
#define OFF BLACK
#define PURPLE VIOLET

class RedPill
{
  public:
    RedPill(uint8_t x, uint8_t y, uint8_t pin=6, uint8_t type=NEO_GRB + NEO_KHZ800);
    ~RedPill();

    void
      on(),
      begin(),
      paint(),
      show(),
      setPixel(uint8_t x, uint8_t y, uint32_t color),
      setPixel(uint8_t x, uint8_t y, uint8_t r, uint8_t g, uint8_t b),
      setBrightness(uint8_t);

    uint8_t
      getXAxisCount(),
      getYAxisCount();

    // RPPixel** getMatrix();
    // RPPixel* getPixel(uint8_t x, uint8_t y);

    static
      uint32_t
        Color(uint8_t r, uint8_t g, uint8_t b);

  private:
    RPPixel** matrix;
    Adafruit_NeoPixel* strip;
    uint8_t xAxisCount;
    uint8_t yAxisCount;
    uint16_t totalLEDs;

};

