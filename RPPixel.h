/*----------------------------------------------------------------------------
  This file is part of the RedPill library.

  Licence: MIT/X11 (see LICENCE file)
  Author: 0x783czar (Diego Carrión)
  ----------------------------------------------------------------------------*/

#include <Adafruit_NeoPixel.h>

class RPPixel
{
  public:
    RPPixel(uint8_t x, uint8_t y, uint16_t serial, Adafruit_NeoPixel *strip);
    ~RPPixel();

    void
      setColor(uint32_t color),
      setColor(uint8_t r, uint8_t g, uint8_t b),
      off();
    uint8_t getXPosition();
    uint8_t getYPosition();
    uint16_t getSerialNumber();

    private:
      Adafruit_NeoPixel* strip;
      uint8_t xPosition;
      uint8_t yPosition;
      uint16_t serialNumber;
};
